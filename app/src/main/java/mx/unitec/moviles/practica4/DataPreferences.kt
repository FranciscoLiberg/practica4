package mx.unitec.moviles.practica4

import android.content.Context
import android.preference.PreferenceManager

private const val  PREF_RFC = "dataFRC"

object DataPreferences {

    fun setStoredRFC(context: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        return prefs.getString(PREF_RFC, "")!!
    }

    fun setStoredRFC(context: Context, query: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PREF_RFC, query)
            .apply()
    }

    fun getStoredRFC(mainActivity: MainActivity) {

    }

}